public static class ArrayManipulator
{
    public static void RotateArray(int[] arrayToRotate, int numberOfRotatingElements)
    {
    	if (arrayToRotate == null || arrayToRotate.Length == 0 || numberOfRotatingElements == 0)
    	{
    		return;
    	}
    	
    	int length = arrayToRotate.Length;
    	int actualNumberOfRotatingElements = numberOfRotatingElements % length;
    
    	if (actualNumberOfRotatingElements == 0)
    	{
    		return;
    	}
    	
    	if(actualNumberOfRotatingElements < 0)
    	{
    		actualNumberOfRotatingElements += length;
    	}
    
    	//First we fully reverse the array
    	ReverseArray(arrayToRotate, 0, length - 1);
    	//Then we reverse only the items to be rotated from the start
    	ReverseArray(arrayToRotate, 0, actualNumberOfRotatingElements - 1);
    	//Then we reverse all the items not to be rotated to the end
    	ReverseArray(arrayToRotate, actualNumberOfRotatingElements, length - 1);
    }
    
    public static void ReverseArray(int[] array, int start, int end)
    {
    	if (arrayToReverse == null || arrayToReverse.Length == 0 || 
    	    start < 0 || end >= arrayToReverse.Length || start >= end)
    	{
    		return;
    	}
    	
    	//we start at the extremes and work towards the middle of the array
    	//we use a temp variable to store one of the values when swapping
    	while(start < end)
    	{
    		int temp = array[start];
    		array[start] = array[end];
    		array[end] = temp;
    		start++;
    		end--;
    	}
    }
}