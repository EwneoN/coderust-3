public class UnderpantsGnome
{
    public static Result Phase2(int[] sackOfUnderpants)
    {
    	if (sackOfUnderpants == null || sackOfUnderpants.Length <= 1)
    	{
    		return null;
    	}
    	
    	
    	if(sackOfUnderpants.Length == 2)
    	{
    		return new Result
    		{
    			Buy = sackOfUnderpants[0],
    			Sell = sackOfUnderpants[1]
    		};
    	}
    
    	int bestBuyPrice = sackOfUnderpants[0];
    	int bestSellPrice = sackOfUnderpants[1];
    	int bestProfit = bestSellPrice - bestBuyPrice;
    
    	for(int i = 1; i < sackOfUnderpants.Length; i++)
    	{
    		int currentPriceOfUnderpants = sackOfUnderpants[i];
    		int potentialProfit = currentPriceOfUnderpants - bestBuyPrice;
    		
    		if(potentialProfit > bestProfit)
    		{
    			bestProfit = potentialProfit;
    			bestSellPrice = currentPriceOfUnderpants;
    		}
    		
    		if(currentPriceOfUnderpants < bestBuyPrice)
    		{
    			bestBuyPrice = currentPriceOfUnderpants;
    		}
    	}
    	
    	return new Result
    	{
    		//we work out Buy this way as bestBuy may be different to what was used
    		//this is because the bestBuy price does not mean it gets the best profit
    		Buy = bestSellPrice - bestProfit,
    		Sell = bestSellPrice
    	};
    }
    
    public class Result
    {
    	public int Buy { get; set; }
    	public int Sell { get; set; }
    }
}