public class ZeroMover
{
    public static void MoveZerosToTheLeft(int[] array)
    {
    	if(array == null || array.Length == 0)
    	{
    		return;
    	}
    
    	int arrayLength = array.Length;
    	int readIndex = arrayLength - 1;
    	int writeIndex = arrayLength - 1;
    	
    	while(writeIndex >= 0)
    	{
    		if(readIndex < 0)
    		{
    			array[writeIndex--] = 0;
    		}
    		else if(array[readIndex] == 0)
    		{
    			readIndex--;
    		}
    		else
    		{
    			array[writeIndex--] = array[readIndex--];
    		}
    	}
    }
}