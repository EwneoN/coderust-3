public class HighestAndLowestIndexFinder
{
    public static int FindLowestIndex(int[] arrayToSearch, int searchTerm)
    {
    	if (arrayToSearch == null || arrayToSearch.Length == 0)
    	{
    		return -1;
    	}
    	
    	int arrayLength = arrayToSearch.Length;
    	
    	if(arrayToSearch[0] == searchTerm)
    	{
    		return 0;
    	}
    
    	int startIndex = 0;
    	int stopIndex = arrayLength - 1;
    	int low = -1;
    
    	while(startIndex <= stopIndex)
    	{
    		int currentWindowSize = stopIndex - startIndex;
    		int currentIndex = currentWindowSize > 1
    			? (startIndex + (currentWindowSize / 2))
    			: startIndex;
    			
    		int currentItem = arrayToSearch[currentIndex];
    			
    		if(currentItem == searchTerm)
    		{
    			if(low == -1 || currentIndex < low)
    			{
    				low = currentIndex;
    			}
    			
    			stopIndex = currentIndex - 1;
    		}
    		else if (currentItem > searchTerm)
    		{
    			stopIndex = currentIndex - 1;		
    		}
    		else
    		{
    			startIndex = currentIndex + 1;
    		}
    	}
    
    	return low;
    }
    
    public static int FindHighestIndex(int[] arrayToSearch, int searchTerm)
    {
    	if (arrayToSearch == null || arrayToSearch.Length == 0)
    	{
    		return -1;
    	}
    
    	int arrayLength = arrayToSearch.Length;
    
    	if (arrayToSearch[arrayLength - 1] == searchTerm)
    	{
    		return arrayLength - 1;
    	}
    
    	int startIndex = 0;
    	int stopIndex = arrayLength - 1;
    	int high = -1;
    
    	while (startIndex <= stopIndex)
    	{
    		int currentWindowSize = stopIndex - startIndex;
    		int currentIndex = currentWindowSize > 1
    			? (startIndex + (currentWindowSize / 2))
    			: startIndex;
    
    		int currentItem = arrayToSearch[currentIndex];
    
    		if (currentItem == searchTerm)
    		{
    			if(currentIndex > high)
    			{
    				high = currentIndex;
    			}
    
    			startIndex = currentIndex + 1;
    		}
    		else if (currentItem > searchTerm)
    		{
    			stopIndex = currentIndex - 1;
    		}
    		else
    		{
    			startIndex = currentIndex + 1;
    		}
    	}
    
    	return high;
    }
}