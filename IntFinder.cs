public class IntFinder
{
    public static int FindMissingInt(int[] intArray)
    {
    	int min = 0;
    	int max = 0;
    	int actual = 0;
    
    	for (int i = 0; i < intArray.Length; i++)
    	{
    		actual += intArray[i];
    
    		if (i == 0 || intArray[i] < min)
    		{
    			min = intArray[i];
    		}
    
    		if (intArray[i] > max)
    		{
    			max = intArray[i];
    		}
    	}
    
    	int expected = FindArithmeticSeries(intArray.Length + 1, min, max);
    	
    	return expected - actual;
    }
    
    private static int FindArithmeticSeries(int n, int min, int max)
    {
    	return (n * (min + max)) / 2;
    }
}