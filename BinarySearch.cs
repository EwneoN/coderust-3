public class BinarySearch
{
    // takes a sorted array. supports rotated sorted arrays
    public static int SearchForInt(int searchTerm, int[] itemsToSearch)
    {
    	int loopCount = 0;
    	
    	if (itemsToSearch == null || itemsToSearch.Length == 0)
    	{
    		Trace.WriteLine($"Loops: {loopCount}");
    		return -1;
    	}
    
    	if (itemsToSearch[0] == searchTerm)
    	{
    		Trace.WriteLine($"Loops: {loopCount}");
    		return 0;
    	}
    
    	if (itemsToSearch[itemsToSearch.Length - 1] == searchTerm)
    	{
    		Trace.WriteLine($"Loops: {loopCount}");
    		return itemsToSearch.Length - 1;
    	}
    	
    	int startIndex = 1;
    	int stopIndex = itemsToSearch.Length - 2;
    	
    	loopCount = 1;
    	
    	while(startIndex <= stopIndex)
    	{		
    		int currentWindowSize = stopIndex - startIndex;
    		int currentIndex = currentWindowSize > 1 ? (startIndex + (currentWindowSize / 2)) : startIndex;
    		int currentItem = itemsToSearch[currentIndex];
    		
    		if (currentItem == searchTerm)
    		{
    			Trace.WriteLine($"Loops: {loopCount}");
    			return currentIndex;
    		}
    		
    		int firstItem = itemsToSearch[startIndex];
    		int lastItem = itemsToSearch[stopIndex];
    		
		    // check if current item is greater than search term, if so previous index becomes new stop index
    		if(firstItem < currentItem && searchTerm < currentItem && searchTerm >= firstItem)
    		{
    			stopIndex = currentIndex - 1;
    		}
		    // check if current item is less than search term, if so next index becomes new start index
    		else if(currentItem < lastItem && searchTerm > currentItem && searchTerm <= lastItem)
    		{
    			startIndex = currentIndex + 1;
    		}
    		// check if current item is less than first item, if so previous index becomes new stop index. 
    		// this check is needed to handle rotated arrays 
    		// in this case the first item in our current window is greater than the current item  
    		else if(firstItem > currentItem)
    		{
    			stopIndex = currentIndex - 1;
    		}
    		// check if current item is greater than last item, if so next index becomes new start index. 
    		// this check is needed to handle rotated arrays 
    		// in this case the last item in our current window is less than the current item
    		else if(lastItem < currentItem)
    		{
    			startIndex = currentIndex + 1;
    		}
    		
    		loopCount++;
    	}
    
    	Trace.WriteLine($"Loops: {loopCount++}");
    	return -1;
    }
}