public class IntervalMerger
{
    public static List<Interval> MergeIntervals(List<Interval> intervals)
    {
    	Interval mergeTarget = intervals[0];
    	List<Interval> newIntervals = new List<Interval> { mergeTarget };
    
    	for(int i = 1; i < intervals.Count;i++)
    	{
    		if(mergeTarget.End >= intervals[i].Start)
    		{			
    			mergeTarget.End = intervals[i].End;
    		}
    		else
    		{
    			mergeTarget = intervals[i];
    			newIntervals.Add(mergeTarget);
    		}
    	}
    
    	return newIntervals;
    }
    
    public class Interval
    {
    	public int Start { get; set; }
    	public int End { get; set; }
    	
    	public Interval(int start, int end)
    	{
    		Start = start;
    		End = end;
    	}
    
    	public override string ToString()
    	{
    		return $"{Start}-{End}";
    	}
    }
}