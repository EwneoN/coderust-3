public class QuickSorter
{
    public static void QuickSort(int[] arrayToSort)
    {
    	QuickSortRecursive(arrayToSort, 0, arrayToSort.Length - 1);	
    }
     
    private static void QuickSortRecursive(int[] arrayToSort, int startIndex, int stopIndex)
    {
    	if(startIndex < stopIndex)
    	{
    		int partition = Partition(arrayToSort, startIndex, stopIndex);
    		QuickSortRecursive(arrayToSort, startIndex, partition);
    		QuickSortRecursive(arrayToSort, partition + 1, stopIndex);
    	}
    }
    
    private static int Partition(int[] arrayToPartition, int startIndex, int stopIndex)
    {
    	int pivot = arrayToPartition[startIndex];
    	int i = startIndex - 1;
    	int j = stopIndex + 1;
    	
    	while(true)
    	{
    		do
    		{
    			i++;
    		} while (arrayToPartition[i] < pivot);
    
    
    		do
    		{
    			j--;
    		} while (arrayToPartition[j] > pivot);
    		
    		if(i >= j)
    		{
    			return j;
    		}
    		
    		int temp = arrayToPartition[i];
    		arrayToPartition[i] = arrayToPartition[j];
    		arrayToPartition[j] = temp;
    	}
    }
}