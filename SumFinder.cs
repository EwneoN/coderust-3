public class SumFinder
{
    public static bool CanSumValueUsingInput(int[] inputArray, int target)
    {
    	if(inputArray == null || inputArray.Length <= 1)
    	{
    		return false;
    	}
    	
    	if(inputArray[0] + inputArray[inputArray.Length - 1] == target)
    	{
    		return true;
    	}
    	else
    	{
    		if(inputArray.Length == 2)
    		{
    			return false;
    		}
    	}
    	
    	HashSet<int> items = new HashSet<int>
    	{
    		inputArray[0],
    		inputArray[1]
    	};
    	
    	for(int i = 2; i < inputArray.Length; i++)
    	{
    		int diff = target - inputArray[i];
    		
    		if(items.TryGetValue(diff, out int storedValue))
    		{
    			return true;
    		}
    
    		if (!items.TryGetValue(inputArray[i], out int duplicate))
    		{
    			items.Add(inputArray[i]);
    		}
    	}
    	
    	return false;
    }
    
    public static bool CanSumValueUsingInputWithConstantMemory(int[] inputArray, int target)
    {
    	if (inputArray == null || inputArray.Length <= 1)
    	{
    		return false;
    	}
    
    	Array.Sort(inputArray);
    	
    	for (int i = 1, j = inputArray.Length - 2; i < j;)
    	{
    		int sum = inputArray[i] + inputArray[j];
    		
    		if(sum == target)
    		{
    			return true;
    		}
    		
    		if(sum < target)
    		{
    			i++;
    		}
    		else
    		{
    			j--;
    		}
    	}
    
    	return false;
    }
}