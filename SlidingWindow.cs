public class SlidingWindow
{
    public static LinkedList<int> FindMaximumInWindow(int[] items, int windowSize)
    {
    	if(items.Length < windowSize)
    	{
    		return new LinkedList<int>();
    	}
    
    	LinkedList<int> heap = new LinkedList<int>();
    	LinkedList<int> window = new LinkedList<int>();
    
    	for(int i = 0; i < windowSize; i++) 
    	{
    		int currentItem = items[i];
    		
    		// remove all while they are smaller than current item
    		while(window.Any() && currentItem >= items[window.Last.Value])
    		{
    			window.RemoveLast();
    		}
    		
    		window.AddLast(i);
    	}
    	
    	heap.AddLast(items[window.First.Value]);
    
    	for (int i = windowSize; i < items.Length; i++)
    	{
    		int currentItem = items[i];
    
    		// remove all while they are smaller than current item
    		while (window.Any() && currentItem >= items[window.Last.Value])
    		{
    			window.RemoveLast();
    		}
    
    		// remove all while they are not in current window
    		while (window.Any() && window.First.Value <= i - windowSize)
    		{
    			window.RemoveFirst();
    		}
    
    		window.AddLast(i);
    		heap.AddLast(items[window.First.Value]);
    	}
    
    	return heap;
    }
}