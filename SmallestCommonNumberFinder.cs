public class SmallestCommonNumberFinder
{
    public static FindSmallestCommonInt(int[] array1, int[] array2, int[] array3)
    {
    	int i = 0;
    	int j = 0;
    	int k = 0;
    
    	while(i <= array1.Length && j <= array2.Length && k <= array3.Length)
    	{
    		//if all items are the same, then we have found the smallest common int
    		if(array1[i] == array2[j] && array2[j] == array3[k])
    		{
    			return array1[i];
    		}
    		
    		//current item in array1 is smaller than the current items in the other two arrays
    		//so advance index for array1 and continue
    		if(array1[i] <= array2[j] && array1[i] <= array3[k]) 
    		{
    			i++;
    			continue;
    		}
    
    		//current item in array2 is smaller than the current items in the other two arrays
    		//so advance index for array2 and continue
    		if (array2[j] <= array1[i] && array2[j] <= array3[k])
    		{
    			j++;
    			continue;
    		}
    
    		//current item in array3 is smaller than the current items in the other two arrays
    		//so advance index for array3 and continue
    		if (array3[k] <= array1[i] && array3[k] <= array2[j])
    		{
    			k++;
    			continue;
    		}
    	}
    	
    	//if we have gotten here, then we could not find a common number
    	return -1;
    }
}